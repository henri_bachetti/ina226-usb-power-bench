#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# A PYTHON script to display data from the power bench
# Usage : python INA226_powerbench.py usb_device duration
#
 
import sys, time
import serial
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import seaborn as sns

sns.set()
duration = 60
plot = "plot"
voltageData = {}
currentData = {}
voltage = 0
current= 0
maxVoltage = 0.0
minVoltage = 1000000.0
maxCurrent = 0.0
minCurrent = 1000000.0
x_ticks = []

if len(sys.argv) > 2:
	duration = int(sys.argv[2])
if len(sys.argv) > 3:
	plot = sys.argv[3]

start_time = time.time()
line = serial.Serial(sys.argv[1], 230400, timeout=1000, parity=serial.PARITY_NONE)
while time.time() < start_time + duration:
	s = line.readline()
	if not s.startswith('INA226 POWER BENCH') and not s.startswith('read current offset from EEPROM'):
		if s.find('V') != -1:
			t, d = s.split(':')
			dummy, voltage = d.split('=')
			voltage = float(voltage)/1000
			t = int(t)
			voltageData[t] = voltage
			print t, 'V=', voltageData[t]
			if maxVoltage < voltage:
				maxVoltage = voltage
			if minVoltage > voltage:
				minVoltage = voltage
		if s.find('I') != -1:
			t, d = s.split(':')
			dummy, current = d.split('=')
			current = float(current)/10
			t = int(t)
			x_ticks.append(t)
			currentData[t] = current
			print t, 'I=', currentData[t]
			if maxCurrent < current:
				maxCurrent = current
			if minCurrent > current:
				minCurrent = current

voltageSerie = []
currentSerie = []
totalVoltage = 0.0
totalCurrent = 0.0
for t in range(duration * 1000):
	if t in voltageData:
		voltage = voltageData[t]
		voltageSerie.append(voltage)
	totalVoltage += voltage
	if t in currentData:
		print "current change:", current, 'at', t
		current = currentData[t]
		currentSerie.append(current)
	totalCurrent += current

text = "DURATION: %s s\n" % duration
text += "MIN VOLTAGE: %s V\n" % minVoltage
text += "MAX VOLTAGE: %s V\n" % maxVoltage
text += "MIN CURRENT: %s uA\n" % minCurrent
text += "MAX CURRENT: %s uA\n" % maxCurrent
text += "AVG VOLTAGE: %1.2f V\n" % (totalVoltage / (duration * 1000))
text += "AVG CURRENT: %1.2f uA\n" % (totalCurrent / (duration * 1000))
print text

if plot == "plot":
	plt.plot(x_ticks, currentSerie)
elif plot == "bar":
	plt.bar(x_ticks, currentSerie)
plt.ylabel('uA')
plt.yscale('log')
plt.grid(True,which="both")
plt.title('POWER TESTBENCH')
plt.text(200, int(maxCurrent) / 4, text, fontdict={'family': 'serif', 'color':  'darkred', 'weight': 'normal', 'size': 10})
plt.show()

