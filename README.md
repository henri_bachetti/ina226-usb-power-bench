# INA226 POWER BENCH

The purpose of this page is to explain step by step the realization of an USB  power bench based on ARDUINO MEGA2560.

It can measure currents from 0.5µA to 0.8A.

This power bench can transmit the measurements to a PC to draw a graph, or display the current on an OLED display.

The board uses the following components :

 * an ARDUINO MEGA
 * two INA226
 * a small OLED 0.96" display
 * an IRLZ44N MOSFET
 * some passive components
 * the board is powered by the USB.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/07/banc-de-mesure-de-consommation.html