
#define DEBUG_DATA                    0x01
#define DEBUG_INA226H                 0x02
#define DEBUG_INA226L                 0x04
#define DEBUG_INA226                  (DEBUG_INA226H+DEBUG_INA226L)
//#define DEBUG                         DEBUG_INA226L
#define DEBUG                         0

#include <Wire.h>
#include <EEPROM.h>
#include <Bounce2.h>
#include <INA226.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

class MyINA226 : public INA226
{
  public:
    bool begin(uint8_t address = INA226_ADDRESS);
    int16_t readRegister16(uint8_t reg);
    int8_t addr;
};

bool MyINA226::begin(uint8_t address)
{
  INA226::begin(address);
  Wire.begin();
  addr = address;
  return true;
}

int16_t MyINA226::readRegister16(uint8_t reg)
{
  int16_t value;

  Wire.beginTransmission(addr);
#if ARDUINO >= 100
  Wire.write(reg);
#else
  Wire.send(reg);
#endif
  Wire.endTransmission();

  delay(1);

  Wire.beginTransmission(addr);
  Wire.requestFrom(addr, 2);
  while (!Wire.available()) {};
#if ARDUINO >= 100
  uint8_t vha = Wire.read();
  uint8_t vla = Wire.read();
#else
  uint8_t vha = Wire.receive();
  uint8_t vla = Wire.receive();
#endif;
  Wire.endTransmission();

  value = vha << 8 | vla;

  return value;
}

Bounce debouncer = Bounce();
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
MyINA226 ina226_L;
MyINA226 ina226_H;
char *title = "LOW-POWER BENCH";

void checkIna226Config(INA226 *ina)
{
  Serial.print("Mode:                  ");
  switch (ina->getMode())
  {
    case INA226_MODE_POWER_DOWN:      Serial.println("Power-Down"); break;
    case INA226_MODE_SHUNT_TRIG:      Serial.println("Shunt Voltage, Triggered"); break;
    case INA226_MODE_BUS_TRIG:        Serial.println("Bus Voltage, Triggered"); break;
    case INA226_MODE_SHUNT_BUS_TRIG:  Serial.println("Shunt and Bus, Triggered"); break;
    case INA226_MODE_ADC_OFF:         Serial.println("ADC Off"); break;
    case INA226_MODE_SHUNT_CONT:      Serial.println("Shunt Voltage, Continuous"); break;
    case INA226_MODE_BUS_CONT:        Serial.println("Bus Voltage, Continuous"); break;
    case INA226_MODE_SHUNT_BUS_CONT:  Serial.println("Shunt and Bus, Continuous"); break;
    default: Serial.println("unknown");
  }
  
  Serial.print("Samples average:       ");
  switch (ina->getAverages())
  {
    case INA226_AVERAGES_1:           Serial.println("1 sample"); break;
    case INA226_AVERAGES_4:           Serial.println("4 samples"); break;
    case INA226_AVERAGES_16:          Serial.println("16 samples"); break;
    case INA226_AVERAGES_64:          Serial.println("64 samples"); break;
    case INA226_AVERAGES_128:         Serial.println("128 samples"); break;
    case INA226_AVERAGES_256:         Serial.println("256 samples"); break;
    case INA226_AVERAGES_512:         Serial.println("512 samples"); break;
    case INA226_AVERAGES_1024:        Serial.println("1024 samples"); break;
    default: Serial.println("unknown");
  }

  Serial.print("Bus conversion time:   ");
  switch (ina->getBusConversionTime())
  {
    case INA226_BUS_CONV_TIME_140US:  Serial.println("140uS"); break;
    case INA226_BUS_CONV_TIME_204US:  Serial.println("204uS"); break;
    case INA226_BUS_CONV_TIME_332US:  Serial.println("332uS"); break;
    case INA226_BUS_CONV_TIME_588US:  Serial.println("558uS"); break;
    case INA226_BUS_CONV_TIME_1100US: Serial.println("1.100ms"); break;
    case INA226_BUS_CONV_TIME_2116US: Serial.println("2.116ms"); break;
    case INA226_BUS_CONV_TIME_4156US: Serial.println("4.156ms"); break;
    case INA226_BUS_CONV_TIME_8244US: Serial.println("8.244ms"); break;
    default: Serial.println("unknown");
  }

  Serial.print("Shunt conversion time: ");
  switch (ina->getShuntConversionTime())
  {
    case INA226_SHUNT_CONV_TIME_140US:  Serial.println("140uS"); break;
    case INA226_SHUNT_CONV_TIME_204US:  Serial.println("204uS"); break;
    case INA226_SHUNT_CONV_TIME_332US:  Serial.println("332uS"); break;
    case INA226_SHUNT_CONV_TIME_588US:  Serial.println("558uS"); break;
    case INA226_SHUNT_CONV_TIME_1100US: Serial.println("1.100ms"); break;
    case INA226_SHUNT_CONV_TIME_2116US: Serial.println("2.116ms"); break;
    case INA226_SHUNT_CONV_TIME_4156US: Serial.println("4.156ms"); break;
    case INA226_SHUNT_CONV_TIME_8244US: Serial.println("8.244ms"); break;
    default: Serial.println("unknown");
  }
  
  Serial.print("Max possible current:  ");
  Serial.print(ina->getMaxPossibleCurrent());
  Serial.println(" A");

  Serial.print("Max current:           ");
  Serial.print(ina->getMaxCurrent());
  Serial.println(" A");

  Serial.print("Max shunt voltage:     ");
  Serial.print(ina->getMaxShuntVoltage());
  Serial.println(" V");

  Serial.print("Max power:             ");
  Serial.print(ina->getMaxPower());
  Serial.println(" W");
}

#define INA226_H_OHM                0.100 // mohm
#define INA226_H_SHUNT              100 // mohm
#define INA226_L_OHM                10  // ohm
#define INA226_L_SHUNT              10  // ohm
#define MOSFET_GATE                 5
#define BUTTON                      6
#define LED                         13
#define GUI_SWITCH                  8

#define VOLTAGE_MEASUREMENT_PERIOD  2000
#define LOWPOWER_SEND_DATA_DELAY    1000
#define LOWPOWER_MEASUREMENT_PERIOD 2000
#define BUFFER_SIZE                 400

struct buffer
{
  long timeStamp;
  long measurement;
  char type;
};

struct buffer dataBuffer[BUFFER_SIZE];

int dataBufferIndex;

int shuntCurrentOffset;

void enableLowPowerShunt(void)
{
  digitalWrite(MOSFET_GATE, LOW);
}

void disableLowPowerShunt(void)
{
  digitalWrite(MOSFET_GATE, HIGH);
}

void displayTitle(void)
{
  int16_t  x1, y1;
  uint16_t w, h;

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.getTextBounds(title, 0, 0, &x1, &y1, &w, &h);
  display.setCursor((display.width() - w) / 2, 0);
  display.println(title);
  display.println("");
}

void displayMessages(char *msg1, char *msg2)
{
  displayTitle();
  display.println(msg1);
  display.println(msg2);
  display.display();
  delay(2000);
}

void displayCalibration(char *prompt, int current, long t)
{
  displayTitle();
  display.println(prompt);
  display.print("CURRENT: ");
  if (current < 0) {
    display.print('-');
    current = abs(current);
  }
  display.print(current / 10);
  display.print('.');
  display.print(current % 10);
  display.println(" uA");
  display.display();
  delay(t);
}

void wait4Key(char *prompt)
{
  displayTitle();
  display.println(prompt);
  display.display();
  while (debouncer.read() == HIGH) {
      debouncer.update();
  }
  while (debouncer.read() == LOW) {
      debouncer.update();
  }
}

void displayMeasurements(float vbus, float current)
{
  displayTitle();
  display.print("VOLTAGE: ");
  display.print(vbus);
  display.println(" V");
  display.print("CURRENT: ");
  if (current > 1000) {
    display.print(current / 1000, 1);
    display.print(" ");
    display.println("mA");
  }
  else {
    display.print(current, 1);
    display.print(" ");
    display.println("uA");
  }
  display.display();
}

void setup() 
{
  Serial.begin(230400);
  Serial.println("INA226 POWER BENCH");

  pinMode(MOSFET_GATE, OUTPUT);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  disableLowPowerShunt();

  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(GUI_SWITCH, INPUT_PULLUP);
  debouncer.attach(BUTTON);
  debouncer.interval(50);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
#if DEBUG & DEBUG_INA226H
  Serial.println("Initialize INA226_H");
  Serial.println("-----------------------------------------------");
#endif
  ina226_H.begin(0x41);
  ina226_H.configure(INA226_AVERAGES_1, INA226_BUS_CONV_TIME_140US, INA226_SHUNT_CONV_TIME_140US, INA226_MODE_SHUNT_BUS_CONT);
  ina226_H.calibrate(INA226_H_OHM, 0.82);
#if DEBUG & DEBUG_INA226L
  Serial.println("Initialize INA226_L");
  Serial.println("-----------------------------------------------");
#endif
  ina226_L.begin(0x40);
  ina226_L.configure(INA226_AVERAGES_16, INA226_BUS_CONV_TIME_8244US, INA226_SHUNT_CONV_TIME_8244US, INA226_MODE_SHUNT_BUS_CONT);
  ina226_L.calibrate(INA226_L_SHUNT, 0.0082);
#if DEBUG & DEBUG_INA226H
  Serial.print("INA226_H\n");
  checkIna226Config(&ina226_H);
  Serial.print("INA226_L\n");
  checkIna226Config(&ina226_L);
#endif
#if DEBUG & DEBUG_INA226
  Serial.println("-----------------------------------------------");
#endif
  displayMessages("DISCONNECT LOAD", "PRESS TO CALIBRATE");

  EEPROM.get(0, shuntCurrentOffset);
  Serial.print("read current offset from EEPROM: ");
  Serial.println(shuntCurrentOffset);
}

long readBusVoltage(void)
{
#if DEBUG & DEBUG_INA226H
  unsigned long begin = micros();
#endif
  long busVoltage = ina226_H.readRegister16(INA226_REG_BUSVOLTAGE) * 125L / 100L; 
#if DEBUG & DEBUG_INA226H
  unsigned long end = micros();
  Serial.print("time: ");
  Serial.println(end-begin);
#endif
#if DEBUG & DEBUG_INA226H
  Serial.print("Bus voltage:   ");
  Serial.print(busVoltage);
  Serial.println(" mV");
#endif
return busVoltage;
}

long readShuntCurrent(void)
{
  // return shunt current in tenth of µA
#if DEBUG & DEBUG_INA226
  unsigned long begin = micros();
#endif
  // shunt voltage unit is 100nV
  long shuntVoltage = ina226_H.readRegister16(INA226_REG_SHUNTVOLTAGE) * 25L;
  // shunt current unit is 100nA
  long shuntCurrent = (shuntVoltage / INA226_H_SHUNT) * 1000;
#if DEBUG & DEBUG_INA226
  unsigned long end = micros();
  Serial.print("time: ");
  Serial.println(end-begin);
#endif
#if DEBUG & DEBUG_INA226H
  Serial.print("H: Shunt voltage: ");
  Serial.print(shuntVoltage / 10);
  Serial.println(" µV");
  Serial.print("H: Shunt current: ");
  Serial.print(shuntCurrent);
  Serial.println(" µA");
#endif
  return shuntCurrent;
}

long readLowPowerShuntCurrent(void)
{
  // return shunt current in tenth of µA
  // shunt voltage unit is 100nV
  long shuntVoltage = ina226_L.readRegister16(INA226_REG_SHUNTVOLTAGE) * 25L;
  // shunt current unit is 100nA
  long shuntCurrent = (shuntVoltage / INA226_L_SHUNT);
#if DEBUG & DEBUG_INA226L
  Serial.print("L: Shunt voltage: ");
  Serial.print(shuntVoltage / 10);
  Serial.println(" µV");
  Serial.print("L: Shunt current: ");
  Serial.print(shuntCurrent / 10);
  Serial.print(".");
  Serial.print(shuntCurrent % 10);
  Serial.println(" µA");
#endif
  return shuntCurrent;
}

void sendDatas(void)
{
  for (int i = 0 ; i < dataBufferIndex ; i++) {
    Serial.print(dataBuffer[i].timeStamp);
    Serial.print(":");
    if (dataBuffer[i].type == 'V') {
      Serial.print("V=");
      Serial.println(dataBuffer[i].measurement);
    }
    if (dataBuffer[i].type == 'I') {
      Serial.print("I=");
      Serial.println(dataBuffer[i].measurement);
    }
  }
  memset(dataBuffer, 0, sizeof(dataBuffer));
  dataBufferIndex = 0;
}

void loop()
{
  long currentTime = millis();
  static long periodicMeasurementTime;
  static long highPowerMeasurementTime;
  static long lowPowerShuntCurrent;
  int offset = 0;
  int samples = 0;
  static long busVoltage;
  static long lastBusVoltage;
  static long lastShuntCurrent;

  long shuntCurrent = readShuntCurrent();
  if (periodicMeasurementTime == 0 || periodicMeasurementTime + VOLTAGE_MEASUREMENT_PERIOD < currentTime) {
    busVoltage = readBusVoltage();
    if (busVoltage != lastBusVoltage) {
      if (dataBufferIndex < BUFFER_SIZE) {
        dataBuffer[dataBufferIndex].timeStamp = currentTime;
        dataBuffer[dataBufferIndex].type = 'V';
        dataBuffer[dataBufferIndex++].measurement = busVoltage;
        lastBusVoltage = busVoltage;
#if DEBUG == DEBUG_DATA
        Serial.print("#index=");
        Serial.print(dataBufferIndex);
        Serial.print(" V=");
        Serial.println(busVoltage);
#endif
      }
      else {
        digitalWrite(LED, HIGH);
        sendDatas();
        digitalWrite(LED, LOW);
      }
    }
  }
  if (shuntCurrent < 50000) {
    if (periodicMeasurementTime == 0 || periodicMeasurementTime + LOWPOWER_MEASUREMENT_PERIOD < currentTime) {
      periodicMeasurementTime = currentTime;
      enableLowPowerShunt();
#ifdef DEBUG
      delay(2000);
#else
      delayMicroseconds(20);
#endif
      lowPowerShuntCurrent = readLowPowerShuntCurrent();
      disableLowPowerShunt();
      if (highPowerMeasurementTime + LOWPOWER_SEND_DATA_DELAY < currentTime) {
        sendDatas();
      }
    }
    if (lowPowerShuntCurrent != lastShuntCurrent) {
      if (dataBufferIndex < BUFFER_SIZE) {
        dataBuffer[dataBufferIndex].timeStamp = currentTime;
        dataBuffer[dataBufferIndex].type = 'I';
        dataBuffer[dataBufferIndex++].measurement = lowPowerShuntCurrent;
        lastShuntCurrent = lowPowerShuntCurrent;
#if DEBUG == DEBUG_DATA
        Serial.print("#index=");
        Serial.print(dataBufferIndex);
        Serial.print(" i=");
        Serial.println(lowPowerShuntCurrent);
#endif
      }
      else {
        digitalWrite(LED, HIGH);
        sendDatas();
        digitalWrite(LED, LOW);
      }
    }
    if (digitalRead(GUI_SWITCH) == LOW) {
      displayMeasurements((float)busVoltage / 1000, (float)lowPowerShuntCurrent / 10);
    }
  }
  else {
    if (dataBufferIndex < BUFFER_SIZE) {
      if (shuntCurrent != lastShuntCurrent) {
        dataBuffer[dataBufferIndex].timeStamp = currentTime;
        dataBuffer[dataBufferIndex].type = 'I';
        dataBuffer[dataBufferIndex++].measurement = shuntCurrent;
        lastShuntCurrent = shuntCurrent;
#if DEBUG == DEBUG_DATA
        Serial.print("#index=");
        Serial.print(dataBufferIndex);
        Serial.print(" I=");
        Serial.println(shuntCurrent);
#endif
      }
    }
    else {
      digitalWrite(LED, HIGH);
      sendDatas();
      digitalWrite(LED, LOW);
    }
    highPowerMeasurementTime = millis();
    if (digitalRead(GUI_SWITCH) == LOW) {
      displayMeasurements((float)busVoltage / 1000, (float)shuntCurrent / 10);
    }
  }
  if (digitalRead(GUI_SWITCH) == HIGH) {
    static bool started = false;
    if (!started) {
      displayMessages("SENDING DATA", "TO SERIAL LINE");
      started = true;
    }
  }

  if (digitalRead(GUI_SWITCH) == LOW) {
    debouncer.update();
    if (debouncer.read() == LOW) {
      enableLowPowerShunt();
      delayMicroseconds(2000);
      while (debouncer.read() == LOW) {
        lowPowerShuntCurrent = readLowPowerShuntCurrent();
        offset += lowPowerShuntCurrent;
        samples++;
        displayCalibration("CALIBRATION", lowPowerShuntCurrent, 1000);
        Serial.print("Correcting offset: ");
        Serial.print(lowPowerShuntCurrent);
        Serial.println("");
        debouncer.update();
      }
      shuntCurrentOffset = offset / samples;
      EEPROM.put(0, shuntCurrentOffset);
      displayCalibration("CALIBRATED", shuntCurrentOffset, 2000);
      Serial.print("Stored offset: ");
      Serial.print(shuntCurrentOffset);
      Serial.println("");
      wait4Key("PRESS TO START");
      periodicMeasurementTime = 0;
    }
  }
#if DEBUG & DEBUG_INA226
  delay(2000);
#endif
}

