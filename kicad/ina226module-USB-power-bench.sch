EESchema Schematic File Version 2
LIBS:ina226module-USB-power-bench-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-displays
LIBS:my-microcontrollers
LIBS:my-misc-modules
LIBS:my-switches
LIBS:ina226module-USB-power-bench-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ina226-module U4
U 1 1 59FDAB8B
P 9650 5250
F 0 "U4" H 9700 5600 60  0000 C CNN
F 1 "ina226-module (10R shunt)" H 9850 5900 60  0000 C CNN
F 2 "myModules:CJMCU-ina226" H 9650 5250 60  0001 C CNN
F 3 "" H 9650 5250 60  0000 C CNN
	1    9650 5250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 59FDB1D2
P 7750 5400
F 0 "#PWR01" H 7750 5150 50  0001 C CNN
F 1 "GND" H 7750 5250 50  0000 C CNN
F 2 "" H 7750 5400 50  0000 C CNN
F 3 "" H 7750 5400 50  0000 C CNN
	1    7750 5400
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR02
U 1 1 59FDB2DF
P 10300 5100
F 0 "#PWR02" H 10300 4950 50  0001 C CNN
F 1 "+5V" H 10300 5240 50  0000 C CNN
F 2 "" H 10300 5100 50  0000 C CNN
F 3 "" H 10300 5100 50  0000 C CNN
	1    10300 5100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 59FDB4A8
P 9150 5400
F 0 "#PWR03" H 9150 5150 50  0001 C CNN
F 1 "GND" H 9150 5250 50  0000 C CNN
F 2 "" H 9150 5400 50  0000 C CNN
F 3 "" H 9150 5400 50  0000 C CNN
	1    9150 5400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 5AF39E1F
P 4450 4450
F 0 "#PWR04" H 4450 4200 50  0001 C CNN
F 1 "GND" H 4450 4300 50  0000 C CNN
F 2 "" H 4450 4450 50  0000 C CNN
F 3 "" H 4450 4450 50  0000 C CNN
	1    4450 4450
	-1   0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 5AF39FEC
P 10500 5550
F 0 "C4" H 10525 5650 50  0000 L CNN
F 1 "100nF" H 10525 5450 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Rect_L7_W2.5_P5" H 10538 5400 50  0001 C CNN
F 3 "" H 10500 5550 50  0000 C CNN
	1    10500 5550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 5AF3A27A
P 10500 5800
F 0 "#PWR05" H 10500 5550 50  0001 C CNN
F 1 "GND" H 10500 5650 50  0000 C CNN
F 2 "" H 10500 5800 50  0000 C CNN
F 3 "" H 10500 5800 50  0000 C CNN
	1    10500 5800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5AF3C459
P 9200 7250
F 0 "#PWR06" H 9200 7000 50  0001 C CNN
F 1 "GND" H 9200 7100 50  0000 C CNN
F 2 "" H 9200 7250 50  0000 C CNN
F 3 "" H 9200 7250 50  0000 C CNN
	1    9200 7250
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR07
U 1 1 5AF3C4AC
P 4200 3900
F 0 "#PWR07" H 4200 3750 50  0001 C CNN
F 1 "+5V" H 4200 4040 50  0000 C CNN
F 2 "" H 4200 3900 50  0000 C CNN
F 3 "" H 4200 3900 50  0000 C CNN
	1    4200 3900
	-1   0    0    -1  
$EndComp
$Comp
L +3.3VP #PWR08
U 1 1 5AF3C6DF
P 9100 6750
F 0 "#PWR08" H 9250 6700 50  0001 C CNN
F 1 "+3.3VP" H 9100 6850 50  0000 C CNN
F 2 "" H 9100 6750 50  0000 C CNN
F 3 "" H 9100 6750 50  0000 C CNN
	1    9100 6750
	1    0    0    -1  
$EndComp
$Comp
L +3.3VP #PWR09
U 1 1 5AF3C745
P 4450 3900
F 0 "#PWR09" H 4600 3850 50  0001 C CNN
F 1 "+3.3VP" H 4450 4000 50  0000 C CNN
F 2 "" H 4450 3900 50  0000 C CNN
F 3 "" H 4450 3900 50  0000 C CNN
	1    4450 3900
	-1   0    0    -1  
$EndComp
$Comp
L ina226-module U3
U 1 1 5AF400C5
P 8250 5250
F 0 "U3" H 8300 5600 60  0000 C CNN
F 1 "ina226-module (0.1R shunt)" H 8300 4800 60  0000 C CNN
F 2 "myModules:CJMCU-ina226" H 8250 5250 60  0001 C CNN
F 3 "" H 8250 5250 60  0000 C CNN
	1    8250 5250
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5AF51C78
P 8250 4450
F 0 "R1" V 8330 4450 50  0000 C CNN
F 1 "110" V 8250 4450 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 8180 4450 50  0001 C CNN
F 3 "" H 8250 4450 50  0000 C CNN
	1    8250 4450
	0    -1   -1   0   
$EndComp
$Comp
L R R3
U 1 1 5AF51C7F
P 10000 4150
F 0 "R3" V 10080 4150 50  0000 C CNN
F 1 "9.1K" V 10000 4150 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 9930 4150 50  0001 C CNN
F 3 "" H 10000 4150 50  0000 C CNN
	1    10000 4150
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X02 P2
U 1 1 5AF55EFE
P 9700 3100
F 0 "P2" H 9700 3250 50  0000 C CNN
F 1 "OUT" V 9800 3100 50  0000 C CNN
F 2 "myConnectors:NINIGI_NS25-W2P" H 9700 3100 50  0001 C CNN
F 3 "" H 9700 3100 50  0000 C CNN
	1    9700 3100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P6
U 1 1 5AF56ABD
P 5650 3050
F 0 "P6" H 5650 3150 50  0000 C CNN
F 1 "TEST" V 5750 3050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 5650 3050 50  0001 C CNN
F 3 "" H 5650 3050 50  0000 C CNN
	1    5650 3050
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR010
U 1 1 5AF56C67
P 6050 3150
F 0 "#PWR010" H 6050 2900 50  0001 C CNN
F 1 "GND" H 6050 3000 50  0000 C CNN
F 2 "" H 6050 3150 50  0000 C CNN
F 3 "" H 6050 3150 50  0000 C CNN
	1    6050 3150
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 5AF5B20F
P 6500 7100
F 0 "SW1" H 6650 7210 50  0000 C CNN
F 1 "CALIBRATION" H 6500 7020 50  0000 C CNN
F 2 "mySwitches:µSW" H 6500 7100 50  0001 C CNN
F 3 "" H 6500 7100 50  0000 C CNN
	1    6500 7100
	0    1    1    0   
$EndComp
$Comp
L GND #PWR011
U 1 1 5AF5B4C9
P 6500 7600
F 0 "#PWR011" H 6500 7350 50  0001 C CNN
F 1 "GND" H 6500 7450 50  0000 C CNN
F 2 "" H 6500 7600 50  0000 C CNN
F 3 "" H 6500 7600 50  0000 C CNN
	1    6500 7600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5AF6AAD8
P 10600 4900
F 0 "#PWR012" H 10600 4650 50  0001 C CNN
F 1 "GND" H 10600 4750 50  0000 C CNN
F 2 "" H 10600 4900 50  0000 C CNN
F 3 "" H 10600 4900 50  0000 C CNN
	1    10600 4900
	1    0    0    -1  
$EndComp
$Comp
L IRF540N Q1
U 1 1 5AF6CF98
P 9700 3700
F 0 "Q1" V 9950 3850 50  0000 L CNN
F 1 "IRLZ44N" V 9950 3500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220_Neutral123_Horizontal_LargePads" H 9950 3625 50  0001 L CIN
F 3 "" H 9700 3700 50  0000 L CNN
	1    9700 3700
	0    -1   -1   0   
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 5AF6E979
P 7200 3100
F 0 "P1" H 7200 3250 50  0000 C CNN
F 1 "INPUT" V 7300 3100 50  0000 C CNN
F 2 "myConnectors:NINIGI_NS25-W2P" H 7200 3100 50  0001 C CNN
F 3 "" H 7200 3100 50  0000 C CNN
	1    7200 3100
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5AF6EE17
P 7500 3300
F 0 "#PWR013" H 7500 3050 50  0001 C CNN
F 1 "GND" H 7500 3150 50  0000 C CNN
F 2 "" H 7500 3300 50  0000 C CNN
F 3 "" H 7500 3300 50  0000 C CNN
	1    7500 3300
	1    0    0    -1  
$EndComp
$Comp
L F_Small F1
U 1 1 5AF6FF82
P 7600 3050
F 0 "F1" H 7560 3110 50  0000 L CNN
F 1 "500mA" H 7480 2990 50  0000 L CNN
F 2 "Fuse_Holders_and_Fuses:Fuseholder5x20_horiz_open_inline_Type-I" H 7600 3050 50  0001 C CNN
F 3 "" H 7600 3050 50  0000 C CNN
	1    7600 3050
	-1   0    0    -1  
$EndComp
$Comp
L D_Small D1
U 1 1 5AF6FF89
P 8100 3450
F 0 "D1" H 8050 3530 50  0000 L CNN
F 1 "P6KE33A" H 7950 3370 50  0000 L CNN
F 2 "Diodes_ThroughHole:Diode_DO-41_SOD81_Horizontal_RM10" V 8100 3450 50  0001 C CNN
F 3 "" V 8100 3450 50  0000 C CNN
	1    8100 3450
	0    1    1    0   
$EndComp
$Comp
L GND #PWR014
U 1 1 5AF6FF90
P 8100 3650
F 0 "#PWR014" H 8100 3400 50  0001 C CNN
F 1 "GND" H 8100 3500 50  0000 C CNN
F 2 "" H 8100 3650 50  0000 C CNN
F 3 "" H 8100 3650 50  0000 C CNN
	1    8100 3650
	1    0    0    -1  
$EndComp
$Comp
L ssd1306 U2
U 1 1 5AF707EA
P 9900 7100
F 0 "U2" H 10025 7100 60  0000 C CNN
F 1 "ssd1306" H 10025 7250 60  0000 C CNN
F 2 "myDisplays:SSD1306-128x64-I2C" H 9925 7100 60  0001 C CNN
F 3 "" H 9925 7100 60  0000 C CNN
	1    9900 7100
	1    0    0    -1  
$EndComp
$Comp
L SWITCH-SPDT SW2
U 1 1 5B014331
P 6950 7150
F 0 "SW2" H 6750 7300 50  0000 C CNN
F 1 "GUI" H 7000 7000 50  0000 C CNN
F 2 "mySwitches:DIP-SWITCH" H 6950 7150 50  0001 C CNN
F 3 "" H 6950 7150 50  0000 C CNN
	1    6950 7150
	0    -1   -1   0   
$EndComp
$Comp
L arduino-mega-RESCUE-ina226module-USB-power-bench U1
U 1 1 5B02A2B3
P 5950 3850
F 0 "U1" H 6525 1650 60  0000 C CNN
F 1 "arduino-mega" H 6525 1525 60  0000 C CNN
F 2 "myMicroControllers:ARDUINO-MEGA" H 5950 3850 60  0001 C CNN
F 3 "" H 5950 3850 60  0000 C CNN
	1    5950 3850
	-1   0    0    -1  
$EndComp
Text GLabel 8100 7150 0    60   Input ~ 0
SDA
Text GLabel 8100 6950 0    60   Input ~ 0
SCL
$Comp
L GND #PWR015
U 1 1 5B656ECA
P 4250 5350
F 0 "#PWR015" H 4250 5100 50  0001 C CNN
F 1 "GND" H 4250 5200 50  0000 C CNN
F 2 "" H 4250 5350 50  0000 C CNN
F 3 "" H 4250 5350 50  0000 C CNN
	1    4250 5350
	-1   0    0    -1  
$EndComp
$Comp
L SW_PUSH SW3
U 1 1 5B656FC1
P 4250 4950
F 0 "SW3" H 4400 5060 50  0000 C CNN
F 1 "RESET" H 4250 4870 50  0000 C CNN
F 2 "mySwitches:µSW" H 4250 4950 50  0001 C CNN
F 3 "" H 4250 4950 50  0000 C CNN
	1    4250 4950
	0    1    1    0   
$EndComp
$Comp
L Led_Small D2
U 1 1 5B6570DA
P 6600 4400
F 0 "D2" H 6550 4525 50  0000 L CNN
F 1 "ERROR" H 6450 4300 50  0000 L CNN
F 2 "LEDs:LED-3MM" V 6600 4400 50  0001 C CNN
F 3 "" V 6600 4400 50  0000 C CNN
	1    6600 4400
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR016
U 1 1 5B65724C
P 6600 3800
F 0 "#PWR016" H 6600 3650 50  0001 C CNN
F 1 "+5V" H 6600 3940 50  0000 C CNN
F 2 "" H 6600 3800 50  0000 C CNN
F 3 "" H 6600 3800 50  0000 C CNN
	1    6600 3800
	-1   0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5B657310
P 6600 4050
F 0 "R2" V 6680 4050 50  0000 C CNN
F 1 "4.7K" V 6600 4050 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 6530 4050 50  0001 C CNN
F 3 "" H 6600 4050 50  0000 C CNN
	1    6600 4050
	1    0    0    -1  
$EndComp
Connection ~ 7800 3050
Wire Wire Line
	7800 3850 7800 3050
Wire Wire Line
	7700 3850 7800 3850
Wire Wire Line
	7700 3850 7700 5000
Wire Wire Line
	6600 3900 6600 3800
Wire Wire Line
	6600 5150 6150 5150
Wire Wire Line
	6600 4500 6600 5150
Wire Wire Line
	6600 4200 6600 4300
Wire Wire Line
	4250 5250 4250 5350
Wire Wire Line
	4250 3950 4250 4650
Wire Wire Line
	4650 3950 4250 3950
Connection ~ 10250 4800
Wire Wire Line
	10600 4800 10600 4900
Wire Wire Line
	9900 3600 10250 3600
Connection ~ 9100 3600
Wire Wire Line
	9100 4800 9250 4800
Wire Wire Line
	9100 3600 9500 3600
Wire Wire Line
	8900 5000 9250 5000
Wire Wire Line
	8900 3050 8900 5000
Wire Wire Line
	10150 4800 10600 4800
Wire Wire Line
	9100 3150 9100 4800
Wire Wire Line
	9500 3150 9100 3150
Wire Wire Line
	9500 3050 8900 3050
Connection ~ 8400 7150
Connection ~ 8200 6950
Wire Wire Line
	8100 6950 8200 6950
Wire Wire Line
	6150 4950 6500 4950
Wire Wire Line
	7000 4750 6150 4750
Wire Wire Line
	7000 6800 7000 4750
Wire Wire Line
	8100 4450 7200 4450
Wire Wire Line
	8400 4450 10000 4450
Wire Wire Line
	7200 5050 6150 5050
Wire Wire Line
	4450 4350 4650 4350
Wire Wire Line
	4450 4450 4450 4350
Wire Wire Line
	4450 4050 4650 4050
Wire Wire Line
	4450 3900 4450 4050
Wire Wire Line
	4200 4150 4650 4150
Wire Wire Line
	4200 3900 4200 4150
Wire Wire Line
	8100 7150 9300 7150
Wire Wire Line
	8200 7050 9300 7050
Wire Wire Line
	8100 3650 8100 3550
Wire Wire Line
	7700 3050 8100 3050
Wire Wire Line
	8100 3050 8100 3350
Connection ~ 8400 6050
Wire Wire Line
	8400 6050 9800 6050
Connection ~ 8200 5900
Wire Wire Line
	9600 5900 8200 5900
Connection ~ 7700 4800
Wire Wire Line
	10000 4450 10000 4300
Wire Wire Line
	9000 5200 8750 5200
Wire Wire Line
	9000 5800 9000 5200
Wire Wire Line
	7750 5200 7750 5400
Wire Wire Line
	7850 5200 7750 5200
Wire Wire Line
	7700 5000 7850 5000
Connection ~ 8200 6350
Connection ~ 8400 6250
Wire Wire Line
	9100 6950 9300 6950
Wire Wire Line
	9100 6750 9100 6950
Wire Wire Line
	9200 6850 9300 6850
Wire Wire Line
	9200 7250 9200 6850
Wire Wire Line
	10500 5800 10500 5700
Wire Wire Line
	10500 5200 10500 5400
Wire Wire Line
	6150 6350 8200 6350
Wire Wire Line
	6150 6250 8400 6250
Wire Wire Line
	9800 6050 9800 5700
Wire Wire Line
	9600 5900 9600 5700
Wire Wire Line
	10150 5200 10500 5200
Wire Wire Line
	10300 5100 10300 5800
Wire Wire Line
	10300 5800 9000 5800
Connection ~ 10300 5200
Wire Wire Line
	9150 5400 9150 5200
Wire Wire Line
	9150 5200 9250 5200
Wire Wire Line
	8400 5700 8400 7150
Wire Wire Line
	8200 5700 8200 7050
Wire Wire Line
	7200 4450 7200 5050
Wire Wire Line
	6050 3150 6050 3050
Wire Wire Line
	6050 3050 5850 3050
Wire Wire Line
	6500 4950 6500 6800
Wire Wire Line
	6500 7400 6500 7600
Wire Wire Line
	6500 7500 6950 7500
Wire Wire Line
	6950 7500 6950 7400
Connection ~ 6500 7500
Wire Wire Line
	7700 4800 7850 4800
Wire Wire Line
	7400 3050 7500 3050
Wire Wire Line
	8750 4800 8900 4800
Wire Wire Line
	9750 4450 9750 3900
Connection ~ 9750 4450
Wire Wire Line
	10000 3600 10000 4000
Connection ~ 10000 3600
Wire Wire Line
	10250 3600 10250 4800
Connection ~ 8900 4800
Wire Wire Line
	7500 3300 7500 3150
Wire Wire Line
	7500 3150 7400 3150
$EndSCHEMATC
